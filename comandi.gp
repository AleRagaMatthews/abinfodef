#set term qt 1
#set term qt  enhanced font "Helvetica,12"
#splot 'pop.txt' u 1:3:2 w l ti 'Spazio delle fasi 3D'

set term qt 2
set ylabel 'Sharks'
set xlabel 'Minnows'
set term qt  enhanced font "Helvetica,12"
p 'pop.txt' u 5:4 w l ti ''

set term qt 3
set term qt  enhanced font "Helvetica,12"
set ylabel ''
set xlabel 'Time'
p 'pop.txt' u 1:2 w l ti 'Sharks', 'pop.txt' u 1:3 w l ti 'Minnows'

set term qt 4
set term qt  enhanced font "Helvetica,12"
p 'pop.txt' u 1:4 w l ti 'Sharks average', 'pop.txt' u 1:5 w l ti 'Minnows average'

#set term qt 5

#p 'pop.txt' u 1:6 w l ti 'Sharks sigma' 

#set term qt 6

#p'pop.txt' u 1:7 w l ti 'Minnows sigma'

pause-1