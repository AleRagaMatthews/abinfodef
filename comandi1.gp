set terminal gif animate delay 50
set output 'Wa-Tor con pandemia.gif'
stats 'water.txt' nooutput
L=100
D=100
Tmin=0
dT=1
set xrange [:L+1]
set yrange [:D+1]
set key out center bottom horizontal
j=Tmin
do for [i=1:int(STATS_blocks)] {
    j=j+dT
    set title sprintf('Time=%.1f',j)
    p 'water.txt' index (i-1) ps 1 pt 5 lt rgb 'blue' title 'Water',\
    'minnow.txt' index (i-1) ps 1 pt 5 lt rgb 'green' title 'Minnows',\
    'shark.txt' index (i-1) ps 1 pt 5 lt rgb 'red' title 'Sharks',\

    pause 0.1
}

pause-1